﻿using UnityEngine;
using System.Collections;

public class Coin : MonoBehaviour
{
    public bool dead = false;

    AudioSource coinAudio;

    void Start()
    {
        coinAudio = gameObject.GetComponents<AudioSource>()[0];
    }

    void Update()
    {
        transform.Rotate(new Vector3(0, 0, 1f), 5 * 30 * Time.deltaTime);
    }

    float fadeSpeed = 0.3f;
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Car")
        {
            coinAudio.Play();
            //transform.Translate(0, -10f, 0);
            //Destroy(gameObject);

            float fadeY = transform.position.y + 0.05f;

            LeanTween.moveY(gameObject, fadeY, fadeSpeed).setEaseInQuint();
            LeanTween.scale(gameObject, Vector3.zero, fadeSpeed).setEaseInQuint();

            StartCoroutine(SelfDestruct());
        }
    }

    IEnumerator SelfDestruct()
    {
        yield return new WaitForSeconds(4f);

        Destroy(gameObject);
    }
}
