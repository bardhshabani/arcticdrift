﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LevelManager : MonoBehaviour
{
    GameObject menu;
    GameObject panel;
    GameObject kanvas;
    GameObject MuteButton;
    Vector3 menuEndPositon;
    Vector3 menuStartPosition;
    Vector3 kanvasEndPositon;
    Vector3 kanvasStartPosition;

    bool playMoveing = false;

    public string season;
    public string level;

    bool moveing = false;
    float displacement;
    float t = 0;

    RectTransform animationRect;
    float animationStartValue = 0;
    float animationEndValue = 0;
    float animationTime = 1;
    float animationT = 0;
    float animationValue = 0;
    int nextUnfinishedLevel = 0;
    private bool mobile = true;
    private int currentTotalCoins = 0;
    private int maxTotalCoins = 0;
    private Transform seasonUlockedEffect;
    private bool season2Unlocked = false;

    private AudioSource windStartAudio;
    private AudioSource windRepeatAudio;
    private AudioSource UIclickAudio;
    private AudioSource cricketRepeatAudio;

    CarMovement carScript;

    void Start()
    {
        //ResetGameScoresToZero();
        displacement = Screen.height;

        //Get number of coins
        currentTotalCoins = 0;
        maxTotalCoins = 0;
        UpdateCurrentTotalCoins();

        float screenArea = Screen.width * Screen.height;

        //Debug.Log("Screen Area: " + screenArea);



        if (SceneManager.GetActiveScene().name == "Main Menu")
        {
            #region "MainMenu"
            if (mobile)
                ResizeMenuForMobile(screenArea);

            kanvas = GameObject.Find("Kanvas");

            kanvasStartPosition = new Vector3(0, 0, 0);
            kanvasEndPositon = new Vector3(0, 0, 0);


            GameObject.Find("CoinsText").GetComponent<Text>().text = currentTotalCoins.ToString()/* + " / " + maxTotalCoins*/;
            GameObject.Find("CoinsPanel").transform.Translate(new Vector3(-(130f - Screen.width / 2), -130f + Screen.height / 2, 0));

            GameObject.Find("BackPlayButton").transform.Translate(new Vector3(130f - Screen.width / 2, -130f + Screen.height / 2, 0));
            GameObject.Find("GhostDriftText").transform.Translate(new Vector3(0, Screen.height / 4));
            //GameObject.Find("PlayButton").transform.Translate(new Vector3(0, -Screen.height / 6));
            MuteButton = GameObject.Find("Mute");
            MuteButton.transform.Translate(new Vector3(0, -Screen.height / 4/* - 4 * Screen.height / 20*/));
            panel = GameObject.Find("Panel");
            panel.transform.Translate(new Vector3(Screen.width, 0));

            menu = GameObject.Find("MainMenu");
            menu.transform.Translate(new Vector3(Screen.width, 0));

            GameObject levels = GameObject.Find("Levels");
            GameObject back = GameObject.Find("BackButton");
            back.transform.Translate(new Vector3(130f - Screen.width / 2, -130f + Screen.height / 2, 0));


            levels.transform.Translate(new Vector3(0, -displacement, 0));



            //Show max and high scores on level buttons
            //for (int S = 2; S <= 2; S++)
            //{
            //    for (int L = 1; L <= 8; L++)
            //    {
            //        GameObject.Find("S" + S + "L" + L + "Score").GetComponent<Text>().text = PlayerPrefs.GetInt("S" + S + "L" + L + "Highscore").ToString() + "/" + PlayerPrefs.GetInt("S" + S + "L" + L + "Maxscore").ToString();
            //    }
            //}

            //muted = PlayerPrefs.GetInt("Muted") == 0 ? false : true;
            //if (muted)
            //{
            //    muted = false;
            //    Mute();
            //}
            //else
            //    AudioListener.pause = false;
            //Debug.Log("Muted: " + muted);
            #endregion

            Locks = new GameObject[9];
            for (int L = 2; L <= 8; L++)
            {
                Locks[L] = GameObject.Find("Lock" + L);
            }
            LockSeason2 = GameObject.Find("LockSeason2");
            if (currentTotalCoins > 150)
                LockSeason2.SetActive(false);
        }
        else
        {
            carScript = GameObject.Find("Car").GetComponent<CarMovement>();

            //Debug.Log("level = " + level);
            menu = GameObject.Find("PauseMenu");
            menu.transform.Translate(new Vector3(0, -displacement, 0));

            GameObject pause = GameObject.Find("Pause");
            pause.transform.Translate(Screen.width / 2 - 70f, Screen.height / 2 - 70f, 0);
            if (season == "1")
                seasonUlockedEffect = GameObject.Find("SeasonUnlocked").transform;

            Material mat = (Material)Resources.Load("Drift Marks");
            CarMovement car = GameObject.Find("Car").GetComponent<CarMovement>();
            if (/*season == "2"*/ true)
            {
                mat.mainTexture = (Texture)Resources.Load("white 60");
                GameObject.Find("TapToStart").GetComponent<Text>().color = new Color(0f, 0f, 0f);

                //car.rotationFriction = 200f;
                //car.driftCorrneringBound = 0;
                //car.constAcc = 0.00017f;
                //car.maxFrictionOnDrift = 0.00016f;
            }

            if (season == "2")
            {
                GameObject.Find("TapToStart").GetComponent<Text>().color = new Color(255f, 255f, 255f);
            }
            else
                GameObject.Find("CarLights").SetActive(false);

            if (mobile)
                ResizeForMobile(screenArea);
        }

        // 3 shtator 2020 // Mshilja gojen ropqit
        muted = PlayerPrefs.GetInt("Muted") == 0 ? false : true;
        if (muted)
        {
            muted = false;
            Mute();
        }
        else
            AudioListener.pause = false;

        menuEndPositon = menu.transform.position;
        menuStartPosition = menu.transform.position;

        //Update max number of coins in current level
        int numOfCoins = GameObject.FindGameObjectsWithTag("Coin").Length;
        //Debug.Log("numOfCoins = " + numOfCoins);
        PlayerPrefs.SetInt("S" + season + "L" + level + "Maxscore", numOfCoins);

        //Wind audio
        windStartAudio = gameObject.GetComponents<AudioSource>()[0];
        windRepeatAudio = gameObject.GetComponents<AudioSource>()[1];
        UIclickAudio = gameObject.GetComponents<AudioSource>()[2];
        cricketRepeatAudio = gameObject.GetComponents<AudioSource>()[3];

        StartCoroutine(playWindSound());
    }

    private void Awake()
    {
        //GameObject.Find("Car").GetComponent<CarMovement>().UIcolor = new Color(0f, 0f, 0f);
    }

    void UpdateCurrentTotalCoins()
    {
        currentTotalCoins = 0;
        maxTotalCoins = 0;
        for (int S = 1; S <= 2; S++)
        {
            for (int L = 1; L <= 8; L++)
            {
                currentTotalCoins += PlayerPrefs.GetInt("S" + S + "L" + L + "Highscore");
                maxTotalCoins += PlayerPrefs.GetInt("S" + S + "L" + L + "Maxscore");
            }
        }
    }

    //Not called anywhere
    private void ResetGameScoresToZero()
    {
        for (int S = 1; S <= 2; S++)
        {
            for (int L = 1; L <= 8; L++)
            {
                PlayerPrefs.SetInt("S" + S + "L" + L + "Highscore", 0);
            }
        }
    }

    private void ResizeForMobile(float screenArea)
    {
        //Debug.Log("Scale - localScale = " + GameObject.Find("Scale").transform.lossyScale);
        float SA1 = 4096000f; //when scale is 2.5 (S8)
        float SA2 = 815502f;//1638400f; // when scale is 1 (Desktop)

        float x = (screenArea - SA1) * 1.5f / (SA1 - SA2) + 2.5f; //ek. drejtzes scale sipas SA. x=scale;
        //Debug.Log("X: " + x);

        float ratio = x / 2.5f;//screenArea / 4096000f;
        //ratio = 1;
        GameObject.Find("Scale").transform.localScale = new Vector3(2.5f, 2.5f, 1f) * ratio;
        //GameObject.Find("FinishedText").transform.localScale = GameObject.Find("FinishedText").transform.localScale / ratio;
        //GameObject.Find("FinishedText").GetComponent<Text>().fontSize = (int)(40f * ratio);
        //GameObject.Find("FinishedText").GetComponent<RectTransform>().sizeDelta = new Vector2(900, 100) * ratio;
        GameObject.Find("Pause").transform.localScale = new Vector3(3f, 3f, 1f) * ratio;
        GameObject.Find("TapToStart").GetComponent<Text>().fontSize = (int)(100f * ratio);
        GameObject tutorial = GameObject.Find("tutorial");
        if(tutorial != null)
        {
            float scl = 1f;
            tutorial.transform.localScale = new Vector3(scl, scl, 1f) * ratio;
            tutorial.transform.localPosition = new Vector3(0, -300 * ratio, 0);
        }

    }

    private void ResizeMenuForMobile(float screenArea)
    {
        float SA1 = 4096000f; //when scale is 2.5 (S8)
        float SA2 = 815502f;//1638400f; // when scale is 1 (Desktop)

        float x = (screenArea - SA1) * 1f / (SA1 - SA2) + 1.5f;

        //Debug.Log("X: " + x);

        float ratio = x / 1.5f;//screenArea / 4096000f;
        //return;
        GameObject.Find("MainMenu").transform.localScale = new Vector3(1.5f, 1.5f, 1f) * ratio;
        GameObject.Find("StartMenu").transform.localScale = new Vector3(2f, 2f, 1f) * ratio;
        GameObject.Find("GhostDriftText").transform.localScale = GameObject.Find("GhostDriftText").transform.localScale / ratio;// new Vector3(0.5f, 0.5f, 1f);
        GameObject.Find("GhostDriftText").GetComponent<Text>().fontSize = (int)(180f * ratio);

    }

    private float dt = 0;
    private float dtLastFrame = 0;

    float deltaTime = 0.0f;
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.P))
        {
            ScreenCapture.CaptureScreenshot("src5.png", 5);
        }

        dt = Time.realtimeSinceStartup - dtLastFrame;
        dtLastFrame = Time.realtimeSinceStartup;

        deltaTime += (Time.deltaTime - deltaTime) * 0.1f;

        if (moveing && Timer())
        {
            t += dt * 5;

            if (t > 1)
            {
                t = 1;
                moveing = false;
            }

            //menu.transform.position = new Vector3(Mathf.Lerp(menuStartPosition.x, menuEndPositon.x, t), Mathf.Lerp(menuStartPosition.y, menuEndPositon.y, t), Mathf.Lerp(menuStartPosition.z, menuEndPositon.z, t));

            if (playMoveing)
            {
                kanvas.transform.position = new Vector3(Mathf.Lerp(kanvasStartPosition.x, kanvasEndPositon.x, t), Mathf.Lerp(kanvasStartPosition.y, kanvasEndPositon.y, t), Mathf.Lerp(kanvasStartPosition.z, kanvasEndPositon.z, t));

                //panel.transform.position = new Vector3(Mathf.Lerp(kanvasStartPosition.x, kanvasEndPositon.x, t), Mathf.Lerp(kanvasStartPosition.y, kanvasEndPositon.y, t), Mathf.Lerp(kanvasStartPosition.z, kanvasEndPositon.z, t));
            }
            else
            {
                menu.transform.position = new Vector3(Mathf.Lerp(menuStartPosition.x, menuEndPositon.x, t), Mathf.Lerp(menuStartPosition.y, menuEndPositon.y, t), Mathf.Lerp(menuStartPosition.z, menuEndPositon.z, t));
            }
        }
        else if (timer == 0)
        {
            t = 0;
            menuStartPosition = menuEndPositon;
            kanvasStartPosition = kanvasEndPositon;

            playMoveing = false;
        }

        Animations();

        //==Season unlocked animation
        if (levelFinished && moveing == false && season2Unlocked)
        {
            seasonUnlockedAnimationTime += 4 * Time.deltaTime;

            float seasonUnlockedScale = DoubleInterpolation(seasonUnlockedAnimationTime, 0, 1.1f, 1f);
            Debug.Log("seasonUnlockedScale = " + seasonUnlockedScale);
            seasonUlockedEffect.localScale = new Vector3(seasonUnlockedScale, seasonUnlockedScale, 1);
        }
        //==//
    }

    float seasonUnlockedAnimationTime = 0;
    void Animations()
    {
        if (animationRect == null)
            return;

        animationT += Time.deltaTime * 1 / animationTime;
        if (animationTime > 1)
            animationTime = 1;

        animationValue = DoubleInterpolation(animationT, animationStartValue, animationEndValue, animationStartValue);

        animationRect.localScale = new Vector2(animationValue, animationValue);

        if (animationTime > 1)
        {
            animationRect = null;
        }
    }

    void StartAnimation(ref RectTransform _animationRect, float _animationStartValue, float _animationEndValue, float _animationTime)
    {
        animationRect = _animationRect;
        animationStartValue = _animationStartValue;
        animationEndValue = _animationEndValue;
        animationTime = _animationTime;
    }

    float DoubleInterpolation(float f, float start, float middle, float end)
    {
        if (f < 0.8)//.5
        {
            return Mathf.Lerp(start, middle,/* 2 * f */ 1.25f * f);
        }
        else
        {
            //return Mathf.Lerp(middle + (middle - end), end, f);
            return Mathf.Lerp(middle, end, (f - 0.8f) * 5f);
        }
    }

    void OnGUI()
    {
        int w = Screen.width, h = Screen.height;

        GUIStyle style = new GUIStyle();

        Rect rect = new Rect(0, 0, w, h * 2 / 100);
        style.alignment = TextAnchor.UpperRight;
        style.fontSize = h * 2 / 100;
        style.normal.textColor = new Color(0.0f, 0.0f, 0.5f, 1.0f);
        float msec = deltaTime * 1000.0f;
        float fps = Time.timeScale / deltaTime;
        string text = string.Format("TimeScale = {2}      {0:0.0} ms ({1:0.} fps)", msec, fps, Time.timeScale);
        // GUI.Label(rect, text, style);
    }


    public void Play()
    {
        OnUIbuttonClick();

        kanvasStartPosition = kanvas.transform.position;
        kanvasEndPositon = kanvasStartPosition;
        kanvasEndPositon.x -= Screen.width;

        moveing = true;
        playMoveing = true;
    }

    public void BackToPlay()
    {
        OnUIbuttonClick();

        kanvasStartPosition = kanvas.transform.position;
        kanvasEndPositon = kanvasStartPosition;
        kanvasEndPositon.x += Screen.width;

        moveing = true;
        playMoveing = true;
    }

    GameObject[] Locks;
    GameObject LockSeason2;

    private void ShowLevelScoreForSeason(string S)
    {
        for (int L = 1; L <= 8; L++)
        {
            GameObject.Find("S2L" + L + "Score").GetComponent<Text>().text = PlayerPrefs.GetInt("S" + S + "L" + L + "Highscore").ToString() + "/" + PlayerPrefs.GetInt("S" + S + "L" + L + "Maxscore").ToString();
        }
    }

    public void Season(string sezoni)
    {
        OnUIbuttonClick();

        Debug.Log("sezoni: " + sezoni);
        if (!moveing && (sezoni != "Back"))
        {
            season = sezoni;
            menuStartPosition = menu.transform.position;
            menuEndPositon = menuStartPosition;
            menuEndPositon.y += displacement;
            moveing = true;

            //StartAnimation()...
            RectTransform thisAnimationRect = GameObject.Find("DunesButton").GetComponent<RectTransform>();
            //animationStartValue = animationRect.localScale.x;
            //animationEndValue = animationRect.localScale.x * 0.8f;
            //animationTime = 0.2f;

            StartAnimation(ref thisAnimationRect, thisAnimationRect.localScale.x, thisAnimationRect.localScale.x * 0.8f, 0.2f);
        }
        else if (!moveing && sezoni == "Back")
        {
            menuStartPosition = menu.transform.position;
            menuEndPositon.y -= displacement;
            moveing = true;
        }

        ShowLevelScoreForSeason(sezoni);

        for (int L = 8; L > 0; L--)
        {
            int score = PlayerPrefs.GetInt("S" + sezoni + "L" + L + "Highscore");
            //Debug.Log("Score = " + score);

            if (score > 0)
            {
                nextUnfinishedLevel = L + 1;
                break;
            }
            else
                nextUnfinishedLevel = 0;
        }
        int palidhje = 0;
        if (int.TryParse(sezoni, out palidhje))
        {
            //Debug.Log("nextUnfinishedLevel = " + nextUnfinishedLevel);
            for (int L = 2; L <= Mathf.Min(nextUnfinishedLevel, 8); L++)
            {
                //Debug.Log("L = " + L);
                Locks[L].SetActive(false);
            }
        }
        else
        {
            for (int L = 2; L <= 8; L++)
            {
                //Debug.Log("L = " + L);
                Locks[L].SetActive(true);
            }
        }
    }

    public void Level(string _level)
    {
        OnUIbuttonClick();

        if (_level == "Restart")
        {
            Time.timeScale = 1f;
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        }
        else
        {
            if (_level != "Main Menu")
                SceneManager.LoadScene("S" + season + "L" + _level);
            else
            {
                Time.timeScale = 1f;
                SceneManager.LoadScene(_level);
            }
        }

        //Debug.Log(seasonName + "-" + level);
    }

    bool paused = false;
    public void Pause()
    {
        OnUIbuttonClick();

        if (levelFinished)
        {
            //Go to the next level;
            level = (int.Parse(level) + 1).ToString();

            if (int.Parse(level) > 8 /*&& int.Parse(season) < 2*/)
            {
                level = (int.Parse(level) % 8).ToString();
                season = (int.Parse(season) + 1).ToString();
            }
            if (int.Parse(season) <= 2)
                SceneManager.LoadScene("S" + season + "L" + level);
            else
                SceneManager.LoadScene("Main Menu");
        }
        else if (!paused)
        {
            //Debug.Log("Pause!");
            Time.timeScale = 0;
            menuStartPosition = menu.transform.position;
            menuEndPositon.y += displacement;
            paused = true;
            moveing = true;
            //AudioListener.pause = true;
            PauseAudio();
        }
        else
        {
            Time.timeScale = 1f;
            menuStartPosition = menu.transform.position;
            menuEndPositon.y -= displacement;
            paused = false;
            moveing = true;
            //AudioListener.pause = false;
            UnPauseAudio();
        }

    }

    public bool levelFinished = false;
    public void LevelFinished(int highscore)
    {
        menuStartPosition = menu.transform.position;
        menuEndPositon.y += displacement;
        moveing = true;
        timer = 1f;
        levelFinished = true;
        Text lvlFnsh = GameObject.Find("FinishedText").GetComponent<Text>();
        lvlFnsh.color = new Color(255, 255, 255, 1);

        if(level == "8" && season == "2")
            lvlFnsh.text = "All levels finished!\nMore levels coming soon!";
        else
            lvlFnsh.text = "Level " + (/*(int.Parse(season) - 1)* 8 + */ int.Parse(level)) + " Finished";

        PlayerPrefs.SetInt("S" + season + "L" + level + "Highscore", highscore);

        int lastCurrentTotalCoins = currentTotalCoins;
        UpdateCurrentTotalCoins();
        if (lastCurrentTotalCoins < 150 && currentTotalCoins >= 150)
        {
            season2Unlocked = true;
        }
    }

    private bool muted = false;
    public void Mute()
    {
        if (muted)
        {
            if (SceneManager.GetActiveScene().name == "Main Menu")
                MuteButton.GetComponentInChildren<RawImage>().texture = Resources.Load<Texture>("mute");
            AudioListener.pause = false;
            PlayerPrefs.SetInt("Muted", 0);
        }
        else
        {
            if (SceneManager.GetActiveScene().name == "Main Menu")
                MuteButton.GetComponentInChildren<RawImage>().texture = Resources.Load<Texture>("unmute");
            AudioListener.pause = true;
            PlayerPrefs.SetInt("Muted", 1);
        }

        muted = !muted;
    }

    private float timer = 0;
    bool Timer()
    {
        timer -= Time.deltaTime;
        if (timer <= 0)
        {
            timer = 0;
            return true;
        }
        else
            return false;
    }


    IEnumerator playWindSound()
    {
        //Debug.Log("windStartAudio.clip.length = " + windStartAudio.clip.length);
        //yield return new WaitForSeconds(windStartAudio.clip.length);
        if (season == "2")
        {
            windStartAudio.Stop();
            cricketRepeatAudio.Play();
            yield break;
        }

        yield return new WaitUntil(() => windStartAudio.isPlaying == false);
        windRepeatAudio.Play();
    }

    private void OnUIbuttonClick()
    {
        UIclickAudio.Play();
    }

    private void PauseAudio()
    {
        carScript.OnGamePauseAudio();
    }

    private void UnPauseAudio()
    {
        carScript.OnGameResumeAudio();
    }
}