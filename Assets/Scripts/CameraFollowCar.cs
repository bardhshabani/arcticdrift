﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CameraFollowCar : MonoBehaviour
{
    public GameObject car;
    CarMovement carScript;
    Vector3 CarDirection;

    private Vector3 cameraDisplacement;
    private Vector3 firstPoint;
    private Vector3 secondPoint;
    private Vector3 position;
    private Vector3 normalPosition;

    private float y;
    private Camera cam;
    private GameObject childCamera;

    float forwardZ = 1.2f;
    //public float rightLeftX = 0.52f;
    //public float rightLeftZ = 0.15f;
    //public float backwardZ = 0.6f;

    Ray bottomLeftRay;
    Ray topLeftRay;
    Ray bottomRightRay;
    Ray topRightRay;

    Vector3 bottomLeft;
    Vector3 topLeft;
    Vector3 bottomRight;
    Vector3 topRight;

    public GameObject sphere;

    private float xCurrentRotation = 65f /*30f*/;
    private float xTargetRotation;

    private float yCurrentRotation = -90f;
    private float yTargetRotation;

    float initialCameraHeight;
    float targetCameraHeight;


    void Start()
    {
        car = GameObject.Find("Car");
        if(car == null)
            car = GameObject.Find("Car2");
        carScript = car.GetComponent<CarMovement>();

        cameraDisplacement = new Vector3(forwardZ, 0, 0);

        initialCameraHeight = 0.788f/*transform.position.y /*= 0.838f*/;
        y = transform.position.y;
        transform.position = new Vector3(car.transform.position.x + cameraDisplacement.x, transform.position.y, car.transform.position.z);

        childCamera = this.gameObject.transform.GetChild(0).gameObject;
        cam = childCamera.GetComponent<Camera>();

        bottomLeftRay = cam.ViewportPointToRay(new Vector3(0, 0, 0));
        topLeftRay = cam.ViewportPointToRay(new Vector3(0, 1, 0));
        topRightRay = cam.ViewportPointToRay(new Vector3(1, 1, 0));
        bottomRightRay = cam.ViewportPointToRay(new Vector3(1, 0, 0));

        childCamera.transform.Rotate(5f, 0, 0);
        xCurrentRotation += 5f;
    }

    void Update()
    {
        //if (carScript.GameIsPaused)
        //    return;

        CarDirection = carScript.two2three(carScript.GetCarDirectionVector());

        LineViewportIntersection();

        cameraDisplacement = -(position - car.transform.position + normalPosition - car.transform.position);

        //sphere.transform.position = firstPoint;
        //Debug.Log("position = " + position + "\n car = " + car.transform.position);
        SetCameraHeight();

        //Debug.Log("carScript.carVelocity = " + carScript.carVelocity);
        transform.position += carScript.two2three(carScript.carVelocity * 60 * Time.deltaTime) + (cameraDisplacement) * Time.deltaTime / 3;

        transform.position = new Vector3(transform.position.x, y, transform.position.z);

        RotateCamera();
        
    }

    void RotateCamera()
    {
        xTargetRotation = 30f - 5f * Mathf.Sin(carScript.directionR);
        yTargetRotation = -90f + 5f * Mathf.Cos(carScript.directionR);

        float rotX = (xTargetRotation - xCurrentRotation) * Time.deltaTime;
        float rotY = (yTargetRotation - yCurrentRotation) * Time.deltaTime / 2;

        childCamera.transform.Rotate(rotX, rotY, 0);

        xCurrentRotation += rotX;
        yCurrentRotation += rotY;
    }
   
    void SetCameraHeight()
    {
        targetCameraHeight = initialCameraHeight - 0.05f * Mathf.Sin(carScript.directionR);

        y += (targetCameraHeight - y) * Time.deltaTime;
    }

    private Vector2 firstV2, secondV2;
    private void LineViewportIntersection()
    {
        Vector2 carPositionVP = cam.WorldToViewportPoint(car.transform.position);
        Vector2 carDirectionVP = cam.WorldToViewportPoint(car.transform.position + CarDirection.normalized * 0.1f);
        carDirectionVP = carDirectionVP - carPositionVP;

        Debug.DrawLine(PlaneRayIntersection(cam.ViewportPointToRay(carPositionVP - carDirectionVP * 3f)), PlaneRayIntersection(cam.ViewportPointToRay(carPositionVP + carDirectionVP * 6f)), Color.white);

        Vector2[] Points = new Vector2[4];

        Points[0] = LinesIntersection2D('x', 1f, carPositionVP, carDirectionVP);
        Points[1] = LinesIntersection2D('x', 0f, carPositionVP, carDirectionVP);
        Points[2] = LinesIntersection2D('y', 1f, carPositionVP, carDirectionVP);
        Points[3] = LinesIntersection2D('y', 0f, carPositionVP, carDirectionVP);

        //DRAW CAMERA FRAME
        #region
        Debug.DrawLine(PlaneRayIntersection(cam.ViewportPointToRay(new Vector2(0, 0))), PlaneRayIntersection(cam.ViewportPointToRay(new Vector2(1, 0))), Color.red);
        Debug.DrawLine(PlaneRayIntersection(cam.ViewportPointToRay(new Vector2(1, 0))), PlaneRayIntersection(cam.ViewportPointToRay(new Vector2(1, 1))), Color.red);
        Debug.DrawLine(PlaneRayIntersection(cam.ViewportPointToRay(new Vector2(1, 1))), PlaneRayIntersection(cam.ViewportPointToRay(new Vector2(0, 1))), Color.red);
        Debug.DrawLine(PlaneRayIntersection(cam.ViewportPointToRay(new Vector2(0, 1))), PlaneRayIntersection(cam.ViewportPointToRay(new Vector2(0, 0))), Color.red);
        #endregion

        
        Find2FramePoints(Points, carPositionVP, carDirectionVP);
        position = PlaneRayIntersection(cam.ViewportPointToRay(3f / 4f * firstV2 + 1f / 4f * secondV2));

        Vector2 normalDirectionVP = new Vector2(-carDirectionVP.y, carDirectionVP.x);

        Points[0] = LinesIntersection2D('x', 1f, carPositionVP, normalDirectionVP);
        Points[1] = LinesIntersection2D('x', 0f, carPositionVP, normalDirectionVP);
        Points[2] = LinesIntersection2D('y', 1f, carPositionVP, normalDirectionVP);
        Points[3] = LinesIntersection2D('y', 0f, carPositionVP, normalDirectionVP);

        Find2FramePoints(Points, carPositionVP, normalDirectionVP);
        normalPosition = PlaneRayIntersection(cam.ViewportPointToRay((firstV2 + secondV2) / 2f));

        firstPoint = PlaneRayIntersection(cam.ViewportPointToRay(firstV2));
        secondPoint = PlaneRayIntersection(cam.ViewportPointToRay(secondV2));

        //Debug.Log(3f / 4f * firstV2 + 1f / 4f * secondV2);
        //Debug.Log("firstPoint: " + firstV2 + "\nsecondPoint: " + secondV2);
        //Debug.Log("0: " + Points[0] + "\n1: " + Points[1]);
        //Debug.Log("2: " + Points[2] + "\n3: " + Points[3]);
    }

    private void Find2FramePoints(Vector2[] Points, Vector2 carPositionVP, Vector2 carDirectionVP)
    {
        bool first = true;
        for (int i = 0; i < 4; i++)
        {
            if (Points[i].x >= 0f && Points[i].x <= 1f && Points[i].y >= 0f && Points[i].y <= 1f)
            {
                if (first)
                {
                    Points[0] = Points[i];
                    first = false;
                }
                else
                    Points[1] = Points[i];
            }
        }


        if ((carDirectionVP.x > 0) != ((Points[0] - carPositionVP).x > 0) || (carDirectionVP.y > 0) != ((Points[0] - carPositionVP).y > 0))
        {
            firstV2 = Points[0];
            secondV2 = Points[1];
        }
        else
        {
            firstV2 = Points[1];
            secondV2 = Points[0];
        }
    }

    private Vector2 LinesIntersection2D(char c, float value, Vector2 a, Vector2 b)
    {
        if (c == 'x')
        {
            //If lines are NOT parallel
            if (b.x != 0)
                return new Vector2(value, b.y / b.x * (value - a.x) + a.y);
        }
        else if (c == 'y')
        {
            //If lines are NOT parallel
            if (b.y != 0)
                return new Vector2(b.x / b.y * (value - a.y) + a.x, value);
        }

        //If lines are parallel
        return new Vector2(200, 200);
    }

    private Vector3 PlaneRayIntersection(Ray ray)
    {
        return new Vector3(ray.origin.x + (carScript.transform.position.y - ray.origin.y) * ray.direction.x / ray.direction.y, carScript.transform.position.y, ray.origin.z + (carScript.transform.position.y - ray.origin.y) * ray.direction.z / ray.direction.y);
    }
}
