﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrashAudioChild : MonoBehaviour
{
    //AudioSource crashAudio;
    CrashAudioParent parent;

	void Start ()
    {
        //crashAudio = gameObject.GetComponents<AudioSource>()[0];
        parent = gameObject.GetComponentInParent<CrashAudioParent>();
    }
	
	void Update ()
    {
		
	}

    public void OnTriggerEnter(Collider other)
    {
        parent.PullTrigger(other);
    }
}
