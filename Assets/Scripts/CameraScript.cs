﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class CameraScript : MonoBehaviour
{
    public GameObject car;

    Vector2 displacement;
    float displacementMagnitude;
    Vector2 carDirection;
    CarMovement carScript;
    List<Vector2> cameraIntersections;
    float cameraHeight;
    float cameraWidth;

    void Start()
    {
        carScript = car.GetComponent<CarMovement>();
        displacement = new Vector3();
        cameraIntersections = new List<Vector2>();
        cameraHeight = Camera.main.orthographicSize;
        cameraWidth = cameraHeight * Screen.width / Screen.height;
        displacementMagnitude = 0;
        //transform.position = new Vector3(car.transform.position.x, car.transform.position.y, transform.position.z);
        transform.Translate(-0.5f, -0.5f, -0.2f);
    }

    Vector3 target;
    void Update()
    {
        // carDirection = carScript.GetCarDirectionVector().normalized;
        //transform.position = new Vector3(carScript.transform.position.x + 0.8f, transform.position.y, carScript.transform.position.z);
        //if (carScript.LeftPressed || carScript.RightPressed)
        //    displacementMagnitude = 0;

        /*displacementMagnitude += 1 / 6 * CameraPointsDistance() * Time.deltaTime;
        if (displacementMagnitude > 1 / 6 * CameraPointsDistance())
            displacementMagnitude = 1 / 6 * CameraPointsDistance();
        */

        //target = carScript.transform.position + new Vector3(carDirection.x, carDirection.y, 0) * 1 / 4 * CameraPointsDistance();
        //displacement = (target - transform.position)/2;

        //transform.Translate(carScript.carVelocity.x + displacement.x * Time.deltaTime, carScript.carVelocity.y + displacement.y * Time.deltaTime, 0);

        //Debug.DrawLine(new Vector3(car.transform.position.x, car.transform.position.y, -1),
        //      new Vector3(transform.position.x + displacement.x, transform.position.y + displacement.y, -1), Color.red);
    }


    float CameraPointsDistance()
    {
        GetCameraIntersection('x', transform.position.x + cameraWidth);
        GetCameraIntersection('x', transform.position.x - cameraWidth);
        GetCameraIntersection('y', transform.position.y + cameraHeight);
        GetCameraIntersection('y', transform.position.y - cameraHeight);

        Vector2 p1 = new Vector2();
        Vector2 p2 = new Vector2();
        Vector2[] vektort = new Vector2[4];
        float[] vlerat = new float[4];
        int num = 0;
        foreach (Vector2 point in cameraIntersections)
        {
            vektort[num] = point;
            vlerat[num] = Mathf.Sqrt(Mathf.Pow(point.x - transform.position.x, 2) + Mathf.Pow(point.y - transform.position.y, 2));
            num++;
        }
        float temp;
        Vector2 tempV;
        for (int i = 0; i < 4; i++)
        {
            for (int j = i + 1; j < 4; j++)
            {
                if (vlerat[i] > vlerat[j])
                {
                    temp = vlerat[i];
                    vlerat[i] = vlerat[j];
                    vlerat[j] = temp;

                    tempV = vektort[i];
                    vektort[i] = vektort[j];
                    vektort[j] = tempV;
                }
            }
        }
        p1 = vektort[0];
        p2 = vektort[1];

        //Debug.Log(p1 + "  -  " + p2);

        cameraIntersections.Clear();

        return Mathf.Sqrt(Mathf.Pow(p1.x - p2.x, 2) + Mathf.Pow(p1.y - p2.y, 2));
    }

    void GetCameraIntersection(char axis, float value)
    {
        if (axis == 'y')
        {
            float x;
            if (Mathf.Abs(carDirection.y) != 0f)
            {
                x = car.transform.position.x + (carDirection.x) / (carDirection.y)
                * (value - car.transform.position.y);

                cameraIntersections.Add(new Vector2(x, value));
            }
        }
        else if (axis == 'x')
        {
            float y;
            if (Mathf.Abs(carDirection.x) != 0f)
            {
                y = car.transform.position.y + (carDirection.y) / (carDirection.x)
                * (value - car.transform.position.x);

                cameraIntersections.Add(new Vector2(value, y));
            }
        }

    }

    bool HasSmallerDistanceFromCar(Vector2 v1, Vector2 v2)
    {
        float distance1 = Mathf.Pow(v1.x - transform.position.x, 2) + Mathf.Pow(v1.y - transform.position.y, 2);
        float distance2 = Mathf.Pow(v2.x - transform.position.x, 2) + Mathf.Pow(v2.y - transform.position.y, 2);

        if (distance2 > distance1)
            return true;
        else
            return false;
    }
}
