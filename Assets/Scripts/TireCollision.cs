﻿using UnityEngine;
using System.Collections;

public class TireCollision : MonoBehaviour
{
    public bool colliding = true;

    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Road")
            colliding = true;
        //Debug.Log("OnTriggerEnter");
    }

    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Road")
            colliding = false;
        //Debug.Log("OnTriggerExit =================================== " + other.gameObject.name);
    }
}
