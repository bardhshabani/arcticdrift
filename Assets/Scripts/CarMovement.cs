﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

enum Car
{
    Bus,
    Bus2,
    WhiteCar,
    YellowCar
}

public enum Difficulty
{
    Pro,
    Regular,
    Beginer
}

public class CarMovement : MonoBehaviour
{
    public Difficulty difficulty;
    public bool tapControll = true;
    private int carIndex = (int)Car.Bus;
    public bool LeftPressed, RightPressed;
    public bool newRotationMechanism;
    public float speed;
    public float maxSpeed = 0.13f;
    public float constAcc = 0.003f;
    public float angle = 90f;
    public float leftRotation = 0f;
    public float rightRotation = 0f;
    public void addAngle(float _angle) { angle = (angle + _angle + 360) % 360; }
    GameObject sprite;

    public bool tapToStart = true;

    public float minRotationSpeed = 50f;
    public float rotationSpeed;
    public float maxRotationSpeed = 150f; //120f 12mars2018
    private float maxRotationSpeedDrifting = 135f; //Affected by newRotationMechanism
    private float maxRotationSpeedNotDrifting = 120f;
    public float directionR { get { return angle * Mathf.PI / 180; } }
    public float direction { get { return angle; } }

    public bool drifting { get { return (Vector2.Angle(Normalize(velocity), Normalize(acceleration)) > 115 * Time.deltaTime); } }

    private Vector2 velocity;
    public Vector2 carVelocity { get { return velocity; } }
    private Vector2 acceleration;

    private bool driftMode = false;
    public bool GetDriftMode { get { return driftMode; } }
    private float typicalCorrneringTime = 0;
    private float driftCorrneringTime = 0;
    private float driftingInterval = 0;
    public bool dead = false;
    private bool veqniher = true;
    public Rigidbody rb;

    public bool levelFinished = false;

    private Camera cam;

    private List<int> RightTouch;
    private List<int> LeftTouch;

    TireCollision rearLeftTire;
    TireCollision rearRightTire;
    //Transform carTransform;
    private bool hittingWall = false;


    private int coins = 0;
    public int Coins { get { return coins; } }
    public bool coinsPlus = false;
    public float coinsFontSize = 2;
    public int highscore = 0;
    public bool newRecord = false;
    public float newRecordFontSize = 0;


    private AudioSource slidingAudio;
    private AudioSource gasAudio;
    private AudioSource busAudio;
    private AudioSource engineStartAudio;
    LevelManager levelManager;
    public bool GameIsPaused = false;

    void Awake()
    {
        QualitySettings.vSyncCount = 0;
        Application.targetFrameRate = 50;
        getSprite();
    }

    void Start()
    {
        levelManager = GameObject.Find("LevelManager").GetComponent<LevelManager>();
        if (levelManager.season == "1")
        {
            UIcolor = new Color(0, 0, 0);
        }
        else
        {
            UIcolor = new Color(255f, 255f, 255f);
        }

        SetStartVariablesFromDB();
        tapToStart = true;
        newRecord = false;
        rotationSpeed = minRotationSpeed;
        velocity = new Vector2(0f, 0f);
        acceleration = GetCarDirectionVector();
        //Debug.Log(" Transform.childCount: " + gameObject.transform.childCount);
        rb = GetComponent<Rigidbody>();

        cam = GameObject.Find("Main Camera2").GetComponent<Camera>();

        RightTouch = new List<int>();
        LeftTouch = new List<int>();
        tapText = GameObject.Find("TapToStart").GetComponent<Text>();
        tutorial_go = GameObject.Find("tutorial");
        maxSpeed = 0.0045f;//0.0043f
        maxRotationSpeed = maxRotationSpeedNotDrifting;

        rearLeftTire = GameObject.Find("RearLeftTire").GetComponent<TireCollision>();
        rearRightTire = GameObject.Find("RearRightTire").GetComponent<TireCollision>();

        AudioSource[] audioList = gameObject.GetComponents<AudioSource>();
        slidingAudio = audioList[1];
        slidingAudio.Stop();
        slidingAudio.volume = 0f;

        gasAudio = audioList[0];
        gasAudio.Stop();
        //gasAudio.volume = 0.2f;//0.001f;//0.2f
        //gasAudio.pitch = 0.7f;
        gasAudio.loop = true;

        if(audioList.Length > 3)
            busAudio = audioList[3];

        if (audioList.Length > 3)
        {
            engineStartAudio = audioList[2];
            engineStartAudio.volume = 0.001f;//0.2f
        }

        //carTransform = GameObject.Find("CarTransform").GetComponent<Transform>();
        Time.timeScale = 1f;

        if (newRotationMechanism)
        {
            driftCorrneringBound = 0;
            maxRotationSpeedDrifting = 150f;
            maxFrictionOnDrift = 0.00016f;


            switch (difficulty)
            {
                case Difficulty.Pro:
                    rotationFriction = 200f;
                    maxSpeed = 0.0045f;
                    constAcc = 0.00017f;
                    break;
                case Difficulty.Regular:
                    rotationFriction = 400f;
                    maxSpeed = 0.0037f;
                    constAcc = 0.00017f;
                    break;
                case Difficulty.Beginer:
                    rotationFriction = 400f;
                    maxSpeed = 0.0036f;
                    constAcc = 0.00015f;

                    maxFrictionOnDrift = 0.00015f;
                    break;
                default:
                    throw new System.Exception("difficulty level not defined; difficulty = " + difficulty.ToString());
            }
        }
    }

    void getSprite()
    {
        Transform trsf = gameObject.transform;
        for (int i = 0; i < trsf.childCount; i++)
        {
            if (i == carIndex)
            {
                sprite = trsf.GetChild(i).gameObject;
                sprite.SetActive(true);
            }
            else
                trsf.GetChild(i).gameObject.SetActive(false);
        }
    }

    void SetStartVariablesFromDB()
    {
        //LevelManager lm = GameObject.Find("LevelManager").GetComponent<LevelManager>();
        highscore = PlayerPrefs.GetInt("S" + levelManager.season + "L" + levelManager.level + "Highscore");

        //Debug.Log("S" + lm.season + "L" + lm.level + "Highscore: " + highscore);
    }

    void Update()
    {
        //if (GameIsPaused)
        //    return;

        CheckIfDead();

        if (!levelFinished)
            InputMovement();

        if (!dead)
            Move();
        else
            SetRigidBodyVelocity();

        speed = velocity.magnitude;


        CalculateDriftingInterval();

        FadeTapToStart();

        if (!GameIsPaused)
            Audio();

        Animations();
    }

    float newRecordAnimationTime = 0;
    float coinsPlusAnimationTime = 0;
    void Animations()
    {
        if (newRecord)
        {
            newRecordAnimationTime += 4 * Time.deltaTime;
            if (newRecordAnimationTime > 1)
                newRecordAnimationTime = 1;

            newRecordFontSize = DoubleInterpolation(newRecordAnimationTime, 0, 2.4f, 1.8f);
        }

        if (coinsPlus)
        {
            coinsPlusAnimationTime += 4 * Time.deltaTime;
            if (coinsPlusAnimationTime > 1)
            {
                coinsPlusAnimationTime = 0;
                coinsPlus = false;
            }

            coinsFontSize = DoubleInterpolation(coinsPlusAnimationTime, 2, 2.2f, 2);
        }

    }

    float DoubleInterpolation(float f, float start, float middle, float end)
    {
        if (f < 0.5)
        {
            return Mathf.Lerp(start, middle, 2 * f);
        }
        else
        {
            return Mathf.Lerp(middle + (middle - end), end, f);
        }
    }

    private void CheckIfDead()
    {
        if ((Mathf.Abs(rb.rotation.x) > 0.2f || Mathf.Abs(rb.rotation.z) > 0.2f) && !dead)
        {
            dead = true;
            //Debug.Log("Quaternion rotation DEAD ???????????????????????????????????");
        }
    }

    void InputMovement()
    {
        if (tapControll)
        {
            if (Input.GetKeyDown(KeyCode.RightControl) || Input.GetKeyDown(KeyCode.Mouse0))
            {
                LeftPressed = true;
                RightPressed = false;

            }
            else if (Input.GetKeyUp(KeyCode.RightControl) || Input.GetKeyUp(KeyCode.Mouse0))
            {
                LeftPressed = false;
                RightPressed = true;
            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.A) || Input.GetKeyDown(KeyCode.LeftArrow))
                LeftPressed = true;
            if (Input.GetKeyDown(KeyCode.D) || Input.GetKeyDown(KeyCode.RightArrow))
                RightPressed = true;

            if (Input.GetKeyUp(KeyCode.A) || Input.GetKeyUp(KeyCode.LeftArrow))
                LeftPressed = false;
            if (Input.GetKeyUp(KeyCode.D) || Input.GetKeyUp(KeyCode.RightArrow))
                RightPressed = false;

            foreach (Touch touch in Input.touches)
            {
                if (touch.phase == TouchPhase.Began)
                {
                    float mousePosition = cam.ScreenToViewportPoint(touch.position).x;
                    if (mousePosition > 0.5f)
                    {
                        RightPressed = true;
                        RightTouch.Add(touch.fingerId);
                    }
                    else
                    {
                        LeftPressed = true;
                        LeftTouch.Add(touch.fingerId);
                    }
                }
                else if (touch.phase == TouchPhase.Ended)
                {
                    //if this touch made the car move left
                    if (LeftTouch.Contains(touch.fingerId))
                    {
                        LeftPressed = false;
                        LeftTouch.Remove(touch.fingerId);
                    }
                    //else if this touch made the car moe right
                    if (RightTouch.Contains(touch.fingerId))
                    {
                        RightPressed = false;
                        RightTouch.Remove(touch.fingerId);
                    }
                }
            }
        }

        if (Input.anyKeyDown)
        {
            //Debug.Log("Q = (" + rb.rotation.eulerAngles.x + ", " + +rb.rotation.eulerAngles.y + ", " + +rb.rotation.eulerAngles.z + ")");
            //Debug.Log("Q = (" + rb.rotation.w + ", " + rb.rotation.x + ", " + +rb.rotation.y + ", " + +rb.rotation.z + ")");
            if (tapToStart)
            {
                if(engineStartAudio != null)
                    engineStartAudio.Play();
                tapToStart = false;
            }
        }
    }

    #region "Movement"

    void Move()
    {
        //rb.velocity = new Vector3(0, 0, 0);
        if (tapToStart)
            return;

        if (!hittingWall)
            transform.Translate(two2three(velocity * 60 * Time.deltaTime));

        SetDirection();

        SetAcceleration();

        CheckDriftMode();

        if (!levelFinished)
            SetVelocity();
        else
            DecreaseVelocity();

    }

    void SetAcceleration()
    {
        acceleration = GetCarDirectionVector() *
            constAcc * 60 * Time.deltaTime;
        //if (hittingWall)
        //    acceleration = new Vector2(0,0);
    }

    void SetVelocity()
    {
        if (!driftMode)
        {
            velocity = Normalize(GetCarDirectionVector()) * velocity.magnitude;
            //if (hittingWall)
            //    velocity = new Vector2(0, 0);
            //acceleration /= 1f;
        }

        if (!levelFinished)
        {
            if ((rearRightTire.colliding || rearLeftTire.colliding) && !Input.GetKey(KeyCode.Space))
            {
                velocity += acceleration;
                velocity -= GetFriction();
            }
            else
                velocity -= velocity.normalized * acceleration.magnitude / 3;
        }
    }

    Vector2 GetFriction()
    {
        if (velocity.magnitude <= 0)
            return new Vector2(0f, 0f);

        return Normalize(velocity) * (acceleration.magnitude * Mathf.Pow(Mathf.Min(velocity.magnitude, maxSpeed) / maxSpeed, 70f/*100-ndryshuar 27qershor*/) + SlowDownOnDrift() * 60 * Time.deltaTime);
    }

    void SetDirection()
    {
        if (!LeftPressed || levelFinished)
            DecreaseRotationSpeed(ref leftRotation);
        else
            IncreaseRotationSpeed(ref leftRotation);

        if (!RightPressed || levelFinished)
            DecreaseRotationSpeed(ref rightRotation);
        else
            IncreaseRotationSpeed(ref rightRotation);

        if (!hittingWall)
            sprite.transform.Rotate(0, -(leftRotation - rightRotation) * Time.deltaTime, 0);

        addAngle((leftRotation - rightRotation) * Time.deltaTime);
    }

    void IncreaseRotationSpeed(ref float sp)
    {
        if (newRotationMechanism)
        {
            sp += /*300f changed on 17Gusht2018*/ 350f * (sp + maxRotationSpeed / 1.5f) / maxRotationSpeed * Time.deltaTime;
        }
        else
        {
            sp += 300f * (sp + maxRotationSpeed / 1.5f) / maxRotationSpeed * Time.deltaTime;
        }

        if (sp > maxRotationSpeed)
            sp = maxRotationSpeed;
    }

    public float rotationFriction = 0f; //200 - e ndryshume me 12/4/2017// Vendoset te LevelManager.sc
    void DecreaseRotationSpeed(ref float sp)
    {
        //Debug.Log("rotationFriction: " + rotationFriction);
        if (sp <= 0)
            return;

        if (levelFinished)
            sp -= 100 * Time.deltaTime;
        else if (drifting/*driftMode /*drifting - e ndryshume me 33 dhjetor*/)
            sp -= rotationFriction * Time.deltaTime;
        else
            sp -= 350 * Time.deltaTime;

        if (sp < 0)
            sp = 0;
    }

    float deadVelocity;
    private float stopInterval = 0;
    void DecreaseVelocity()
    {
        stopInterval += Time.deltaTime;
        if (stopInterval > 1f)
            stopInterval = 1f;

        velocity = velocity.normalized * Mathf.Lerp(deadVelocity, 0, stopInterval);
    }

    public float driftCorrneringBound = 0.25f; // in seconds
    void CheckDriftMode()
    {
        if (drifting)
        {
            typicalCorrneringTime -= Time.deltaTime;
            if (typicalCorrneringTime < 0)
                typicalCorrneringTime = 0;
        }
        else
        {
            driftCorrneringTime -= Time.deltaTime;
            if (driftCorrneringTime < 0)
                driftCorrneringTime = 0f;
        }

        if (drifting && driftMode == false)
        {
            driftCorrneringTime += Time.deltaTime;
            if (driftCorrneringTime >= driftCorrneringBound)
            {
                driftMode = true;
                typicalCorrneringTime = 0;
            }
        }
        else if (!drifting && driftMode == true)
        {
            typicalCorrneringTime += Time.deltaTime;
            if (typicalCorrneringTime >= 0.07f)
            {
                driftMode = false;
                rotationSpeed = minRotationSpeed;
                driftCorrneringTime = 0;
            }
        }


        if (driftMode && maxRotationSpeed != maxRotationSpeedDrifting)
        {
            maxRotationSpeed = maxRotationSpeedDrifting;
        }
        else if (!driftMode && maxRotationSpeed != maxRotationSpeedNotDrifting)
        {
            maxRotationSpeed = maxRotationSpeedNotDrifting;
        }
    }

    public Vector2 GetCarDirectionVector()
    {
        return new Vector2(Mathf.Cos(directionR), Mathf.Sin(directionR));
    }

    public float maxFrictionOnDrift = 0.00017f;//Vendoset ne LevelManager.cs
    private float SlowDownOnDrift2()
    {
        //float maxFrictionOnDrift = 0.0001f;//0.00017f;

        return Mathf.Lerp(0, maxFrictionOnDrift, driftingInterval / maxDriftingInterval);
    }

    private float SlowDownOnDrift()
    {
        //float maxFrictionOnDrift = 0.0001f;//0.00017f;
        float alpha = Vector2.Angle(Normalize(acceleration), Normalize(velocity));

        return 0.8f * (Mathf.Abs(Mathf.Sin(alpha)) * maxFrictionOnDrift) + 0.2f * SlowDownOnDrift2();
    }

    private float maxDriftingInterval = 1f;
    private void CalculateDriftingInterval()
    {
        if (driftMode && (leftRotation > (maxRotationSpeed - 20f) || rightRotation > (maxRotationSpeed - 20f)))
        {
            driftingInterval += Time.deltaTime;
            if (driftingInterval > maxDriftingInterval)
                driftingInterval = maxDriftingInterval;
        }
        else
        {
            driftingInterval -= 5 * Time.deltaTime;
            if (driftingInterval < 0)
                driftingInterval = 0;
        }
    }

    private void SetRigidBodyVelocity()
    {
        if (veqniher)
        {
            //if (rearLeftTire.colliding || rearRightTire.colliding)
            //{
            rb.velocity = 75 * two2three(velocity);
            //Debug.Log("Rigidbody ACTIVATED!");
            //}
            //else
            //    Debug.Log("Rigidbody so aktivizu!");

            deadVelocity = velocity.magnitude;
            veqniher = false;
        }
        DecreaseVelocity();
        RestartLevel();
    }

    #endregion

    private float restartTimer = 0;
    void RestartLevel()
    {
        restartTimer += Time.deltaTime;
        if (restartTimer > 1 && !levelFinished)
            SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }

    #region "Triggers"
    void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "Road" && !dead)
        {
            dead = true;
            //Debug.Log("OnTriggerExit Kerri - Rruga !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!");
        }
        else if (other.gameObject.tag == "Wall")
        {
            hittingWall = false;
        }
    }

    bool finishOnce = true;
    void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Finish")
        {
            levelFinished = true;
            deadVelocity = velocity.magnitude;
            if (finishOnce)
            {
                // highscore = Mathf.Max(highscore, coins);
                GameObject.Find("LevelManager").GetComponent<LevelManager>().LevelFinished(Mathf.Max(highscore, coins));
                finishOnce = false;
                //Debug.Log("Finish");
                slidingAudio.volume = slidingVolumeMax;
                slidingVolumeTarget = slidingVolumeMax;
            }
        }
        else if (other.gameObject.tag == "Coin")
        {
            //Debug.Log("Coin Trigger Enter");
            if (!other.gameObject.GetComponent<Coin>().dead)
            {
                other.gameObject.GetComponent<Coin>().dead = true;
                coins++;
                coinsPlus = true;
                if (coins == highscore + 1 && highscore != 0)
                {
                    newRecord = true;
                }
                //highscore = Mathf.Max(highscore, coins);
            }
        }
        else if (other.gameObject.tag == "Wall")
        {
            hittingWall = true;
            Debug.Log("hittingWall = true");
        }
    }
    #endregion

    #region "Methods"
    Vector2 Normalize(Vector2 vector)
    {
        if (vector.magnitude != 0)
            return vector / Mathf.Sqrt(vector.x * vector.x + vector.y * vector.y);
        else
            return new Vector2(0, 0);
    }

    public Vector3 two2three(Vector2 vec2)
    {
        return new Vector3(-vec2.y, 0, vec2.x);
    }
    #endregion

    float tapToStartAlpha = 1f;
    Text tapText;
    GameObject tutorial_go;

    void FadeTapToStart()
    {
        if (!tapToStart && tapToStartAlpha != 0)
        {

            tapToStartAlpha -= Time.deltaTime * 7;
            if (tapToStartAlpha < 0)
                tapToStartAlpha = 0;

            tapText.color = new Color(tapText.color.r, tapText.color.g, tapText.color.b, tapToStartAlpha);

            if (tutorial_go != null)
                tutorial_go.SetActive(false);
        }
    }

    #region "Audio"

    private float slidingVolumeMax = 0.17f;//0.005f;//0.3
    private float slidingVolumeTarget = 1f;

    public float gasPitchMax = 0.5f;
    private float gasPitchTarget = 1f;
    bool startGasAudio = true;
    float pitchAdoptionSpeed = 1f;
    float minPitchAdoptionSpeed = 1f/3f; //0.5f
    float maxPitchAdoptionSpeed = 1f/1.5f; //2f
    float pitchTime = 0f;
    float nothing;
    float dropPitch = 1f;
    float dropPitchLow = 0.3f;
    System.Random random = new System.Random();

    void Audio()
    {
        if (!tapToStart)
        {
            if (startGasAudio)
            {
                startGasAudio = false;
                gasAudio.Play();
                if (busAudio != null && !busAudio.isPlaying)
                    busAudio.Play();
            }

            float rotationRatio = Mathf.Abs(leftRotation - rightRotation) / maxRotationSpeed;

            //Propocion i drejt me shpejtsin. Proporcion i drejt me driftin.
            gasPitchTarget = gasPitchMax * velocity.magnitude / maxSpeed * (1.1f + rotationRatio * 1f) * dropPitch;

            if (rotationRatio > 0.7f) //gasPitchTarget > gasPitchMax
            {
                pitchTime += Time.deltaTime * 1.8f;
                if (pitchTime > 1f)
                    pitchTime = 1f;
                pitchAdoptionSpeed = Mathf.Lerp(minPitchAdoptionSpeed, maxPitchAdoptionSpeed, pitchTime);
            }
            else
            {
                pitchTime -= Time.deltaTime * 2;
                if (pitchTime < 0)
                    pitchTime = 0;
                pitchAdoptionSpeed = Mathf.Lerp(minPitchAdoptionSpeed, maxPitchAdoptionSpeed, pitchTime);
            }

            if (levelFinished || dead)
                pitchAdoptionSpeed = 4f;

            if(gasPitchTarget - gasAudio.pitch >= 0)    // pitch decrases x times faster than in increases
                gasAudio.pitch = Mathf.Min(gasPitchMax, gasAudio.pitch + (gasPitchTarget - gasAudio.pitch) * Time.deltaTime * pitchAdoptionSpeed);
            else
                gasAudio.pitch = Mathf.Min(gasPitchMax, gasAudio.pitch + (gasPitchTarget - gasAudio.pitch) * Time.deltaTime * pitchAdoptionSpeed * 10f); 


            if (gasAudio.pitch < 0.4f * gasPitchMax)
            {
                gasAudio.pitch = 0.4f * gasPitchMax;
                if (dead || levelFinished)
                    gasAudio.pitch = 0;
            }

            if (Mathf.Abs(gasPitchTarget - gasAudio.pitch) < gasPitchMax * 0.35f)
            {
                dropPitch = 1f;
            }

            if (gasAudio.pitch >= gasPitchMax)
            {
                //gasAudio.pitch = (gasPitchMax - 0.4f);// + random.Next(20) / 100f;
                dropPitch = dropPitchLow;
            }

            if (busAudio != null)
            {
                if (gasAudio.pitch == 0)
                    //busAudio.pitch = Mathf.SmoothDamp(busAudio.pitch, 0, ref nothing, .2f);
                    busAudio.pitch = 0;
                else
                    busAudio.pitch = gasAudio.pitch + 0.2f;
            }
            //if (gasAudio.pitch >= 1f)
            //    gasAudio.pitch = 0.75f + random.Next(15) / 100f;

            //SLIDING
            if ((!dead && driftMode) || (!dead && levelFinished))
            {
                if (!slidingAudio.isPlaying)
                {
                    slidingAudio.pitch = 1 + random.Next(3) / 10f;
                    slidingAudio.Play();
                }

                float para = slidingVolumeTarget;
                if (levelFinished)
                    slidingVolumeTarget = Mathf.SmoothDamp(slidingVolumeTarget, 0, ref nothing, 0.5f);
                else
                    slidingVolumeTarget = Mathf.Max(0.2f, rotationRatio) * slidingVolumeMax;

                //Debug.Log("slidingVolumeTarget: " + slidingVolumeTarget + " ;;;; para = " + para);
                //slidingVolumeTarget = slidingVolumeMax * pitchAdoptionSpeed / 2;

                //Koha per me kalu volumi prej slidingAudio.volume ne slidingVolumeTarget
                //float volumeChangeSpeed = /*(Koha per me kalu volumi prej 0 ne slidingVolumeMax) =>*/1f * /*Proporcioni =>*/ Mathf.Abs(slidingVolumeTarget - slidingAudio.volume) / slidingVolumeMax;

                slidingAudio.volume = Mathf.Min(slidingVolumeMax, slidingAudio.volume + (slidingVolumeTarget - slidingAudio.volume) * Time.deltaTime * 4f);
            }
            else
                StopAudio(ref slidingAudio, 0.1f, slidingVolumeMax);
        }
    }

    void PlayAudio(ref AudioSource audio, float volSpeed, float maxVolume)
    {
        if (!audio.isPlaying)
            audio.Play();

        if (audio.volume < slidingVolumeMax)
        {
            audio.volume += Time.deltaTime * maxVolume * 1 / volSpeed;
        }
        else
            audio.volume = slidingVolumeMax;
    }

    void StopAudio(ref AudioSource audio, float volSpeed, float maxVolume)
    {
        if (audio.volume > 0.1)
        {
            audio.volume -= Time.deltaTime * maxVolume * 1 / volSpeed;
        }
        else if (audio.isPlaying)
        {
            audio.volume = 0;
            audio.Stop();
        }
    }

    public void OnGamePauseAudio()
    {
        slidingAudio.Pause();
        gasAudio.Pause();
        busAudio.Pause();
        GameIsPaused = true;
    }

    public void OnGameResumeAudio()
    {
        slidingAudio.Play();
        gasAudio.Play();
        busAudio.Play();
        GameIsPaused = false;
    }

    #endregion

    public Color UIcolor;
    void OnGUI()
    {
        int w = Screen.width, h = Screen.height;

        GUIStyle style = new GUIStyle();
        //Coins
        Rect rect = new Rect(0, 0, w, 2 * h * 2 / 100);
        style.alignment = TextAnchor.UpperLeft;
        style.fontSize = 2 * h * 2 / 100;
        style.font = Resources.Load<Font>("VIDEOPHREAK");
        style.normal.textColor = UIcolor;// new Color(0.0f, 0.0f, 0.0f, 1.0f);
        string text = string.Format("   Coins:");
        GUI.Label(rect, text, style);

        //Debug.Log("Height: " + h);
        //Formula for predicting the position of score Rect. (initial font size=24)
        float xRect = 120f * style.fontSize / 24f;
        Rect rect2 = new Rect(xRect/*120*/, 0, w, 2 * h * 2 / 100);
        style.fontSize = (int)(coinsFontSize * h * 2 / 100);
        text = string.Format("{0}", coins);
        GUI.Label(rect2, text, style);

        //Highscore
        style.alignment = TextAnchor.UpperRight;
        style.fontSize = 2 * h * 2 / 100;
        text = string.Format("Best: {0}    ", highscore);
        GUI.Label(rect, text, style);

        //NewRecord
        if (newRecord)
        {
            style.alignment = TextAnchor.UpperCenter;
            style.fontSize = (int)(newRecordFontSize * h * 2 / 100);
            text = string.Format("New Record");
            GUI.Label(rect, text, style);
        }
    }
}