﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CrashAudioParent : MonoBehaviour
{
    GameObject Car;
    AudioSource crashAudio;
    public float displacement = 0;

    void Start()
    {
        Car = GameObject.Find("car3D");
        crashAudio = gameObject.GetComponents<AudioSource>()[0];
    }

    void LateUpdate()
    {
        transform.position = Car.transform.position + Car.transform.rotation * new Vector3(displacement, 0, 0);
        transform.rotation = Car.transform.rotation;

        //Debug.Log(Car.transform.rotation + " vs " + transform.rotation);
    }


    public void PullTrigger(Collider other)
    {
        //return;
        if (other.tag == "Car" || other.tag == "Finish")
        {
            //Debug.Log("kerri collider");
            return;
        }

        if (!crashAudio.isPlaying)
        {
            System.Random random = new System.Random();
            crashAudio.pitch = 0.5f + (float)random.Next(100) / 100f;
            crashAudio.Play();
            Debug.Log("crashAudio.pitch = " + crashAudio.pitch);
        }
    }
}
