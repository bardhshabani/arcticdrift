﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class DrawLineRenderer : MonoBehaviour
{
    private LineRenderer lineRenderer;
    public GameObject tire;
    public string tireName;
    private TireCollision tireScript;
    private CarMovement carMovementScript;
    public List<Vector3> points;
    private bool first = true;
    private bool dead = false;
    private bool wasDead = false;

    void Start()
    {
        dead = false;
        carMovementScript = GameObject.Find("Car").GetComponent<CarMovement>();
        lineRenderer = GetComponent<LineRenderer>();
        lineRenderer.SetWidth(.003f, .003f);

        if (tire == null)
            tire = GameObject.Find(tireName);
        tireScript = tire.GetComponent<TireCollision>();

        points.Clear();
    }

    void LateUpdate()
    {
        if (first)
        {
            points.Clear();
            lineRenderer.SetVertexCount(points.Count);
        }

        if (!tireScript.colliding && !dead && !first)
        {
            dead = true;
        }

        if (first == false && !carMovementScript.GetDriftMode && !carMovementScript.levelFinished)
            dead = true;

        if (dead && !wasDead && !carMovementScript.levelFinished)
        {
            Instantiate(gameObject);
        }

        if ((carMovementScript.GetDriftMode || carMovementScript.levelFinished) && !dead && !carMovementScript.dead)
        {
            if (first)
            {
                points.Clear();
                first = false;
            }

            //if(!carMovementScript.dead)
            //float ypsi = tire.transform.lossyScale.y;
            Vector3 pika = tire.transform.position; // - new Vector3(0, ypsi, 0); //new Vector3(0, 0.002f, 0);
            points.Add(pika);

            lineRenderer.SetVertexCount(points.Count);

            //if (!carMovementScript.dead)
            lineRenderer.SetPosition(points.Count - 1, pika /*+ new Vector3(0, 0.0010f, 0)*/);
        }

        wasDead = dead;
    }
}
