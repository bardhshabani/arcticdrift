﻿using UnityEngine;
using System.Collections;

public class RotateWheel : MonoBehaviour
{
    public bool LeftRightTurn;
    public GameObject car;
    CarMovement carScript;

    private bool left = true;
    private bool right = true;

    private float angle = 20f;

    void Start ()
    {
        car = GameObject.Find("Car");
        if (car == null)
            car = GameObject.Find("Car2");

        carScript = car.GetComponent<CarMovement>();
    }

    Vector3 turnRotationAxis = new Vector3(0, 1, 0);
    Vector3 forwardRotationAxis = new Vector3(1, 0, 0); // new Vector3(1, 0, 0)


    void Update ()
    {

        if(LeftRightTurn)
        {

            if(carScript.LeftPressed && left)
            {
                left = false;
                transform.Rotate(turnRotationAxis, -angle);
            }
            else if(!carScript.LeftPressed && !left)
            {
                left = true;
                transform.Rotate(turnRotationAxis, angle);
            }

            if (carScript.RightPressed && right)
            {
                right = false;
                transform.Rotate(turnRotationAxis, angle);
            }
            else if (!carScript.RightPressed && !right)
            {
                right = true;
                transform.Rotate(turnRotationAxis, -angle);
            }
        }
        else if (!carScript.levelFinished)
            transform.Rotate(forwardRotationAxis, 5 * 360 * Time.deltaTime);
    }
}
